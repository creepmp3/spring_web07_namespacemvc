package kr.co.hbilab.app;

public interface Character{
    
    public void walk();
    public void eat(String type);
    public void attack(Object obj);
    public void get(Object obj);

}
