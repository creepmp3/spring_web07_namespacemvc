package kr.co.hbilab.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySqlDAO implements CommonDAO{
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    StringBuffer sql = new StringBuffer();
    
    @Override
    public Connection connect() {
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://192.168.0.80:3306/testdb";
        String username = "scott";
        String password = "tiger";
        
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
        }catch(ClassNotFoundException e){
            System.out.println("드라이버 로딩 실패");
        }catch(SQLException e){
            System.out.println("DB 연결 실패");
        }
        
        return conn;
    }
    
    @Override
    public ArrayList<DeptDTO> selectAll() {
        System.out.println("MySQL 전체조회 실행");
        return new ArrayList<DeptDTO>();
    }
    
}
